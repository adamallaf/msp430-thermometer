/*
*   date:   02.04.2016
*   update: 19.01.2019
*
*   Author: Adam Allaf
*
*/
#include <msp430.h>
#include "i2c.h"

// functions writen in Assembly
extern int bin2dec(int val);    // bin2dec.s
extern void TMP102_Init();      // tmp102_init.s

// display pins
#define CATS    0x0F
#define P1BITS  0x30
#define P2BITS  0x3F

// port 1
#define segC    BIT5
#define segE    BIT4
// port 2
#define dp      BIT0
#define segG    BIT1
#define segD    BIT2
#define segF    BIT3
#define segA    BIT5
#define segB    BIT4

// display refresh period [ms]
#define DISP_T  3

#define MINUS   10
#define D_OFF   11

// TMP102 commands
#define TMP102_TREG       0x00
#define TMP102_CONFREG    0x01
#define TMP102_THYSTREG   0x02
#define TMP102_TOSREG     0x03

// Conversion resolution settings
#define RES09BITS       0x00
#define RES10BITS       0x20
#define RES11BITS       0x40
#define RES12BITS       0x60

// I2C devices addresses (7-bit)
#define FM75            0x48
#define PCF8574A        0x38
#define TMP102          0x48

// counters
volatile unsigned int dr_counter = 0;       // display refresh time counter
volatile unsigned int du_counter = 0;       // display update time counter
volatile unsigned int tx_counter = 0, rx_counter = 0; // I2C counters

volatile unsigned char n, digit;

// Port 1 segments to turn on for specified digit.
const char num1[14] = {
    segC | segE,    // 0
    segC,           // 1
    segE,           // 2
    segC,           // 3
    segC,           // 4
    segC,           // 5
    segC | segE,    // 6
    segC,           // 7
    segC | segE,    // 8
    segC,           // 9
    0,              // -
    0,              //
    segE,           // E
    segE            // r
};

// Port 2 segments to turn on for specified digit.
const char num2[14] = {
    segA | segB | segD | segF,          // 0
    segB,                               // 1
    segA | segB | segD | segG,          // 2
    segA | segB | segD | segG,          // 3
    segB | segG | segF,                 // 4
    segA | segD | segG | segF,          // 5
    segA | segD | segG | segF,          // 6
    segA | segB,                        // 7
    segA | segB | segD | segG | segF,   // 8
    segA | segB | segD | segG | segF,   // 9
    segG,                               // -
    0,                                  //
    segA | segD | segF | segG,          // E
    segG                                // r
};

char dd[4] = {0, 0, 11, 11};

extern volatile unsigned int I2C_Tx, I2C_Rx;

extern char rxdata[];
extern char txdata[];


int main(){
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog

    // 1MHz clock
    DCOCTL = CALDCO_1MHZ;
    BCSCTL1 = CALBC1_1MHZ;

    I2C_Tx = 0;
    I2C_Rx = 0;
    n = 0;
    digit = 1;

    P1DIR = 0;
    P1DIR |= P1BITS | CATS;
    P1OUT |= CATS;

    P2DIR = 0;
    P2DIR |= P2BITS;
    P2OUT = 0;

    I2C_Init(TMP102);

    // timer0 init
    // ID_0 -> /1 divider
    // MC_1 -> up mode, count up to CCR0
    // TASSEL_2 -> SMCLK as timer clock source
    TA0CTL |= TASSEL_2 | ID_0 | MC_1 | TAIE;
    TA0CCR0 = 0x3e8;    // T = 1ms
    WRITE_SR(GIE);      // enable interrupts

    TMP102_Init();

    // when timer count fron CCR0 ot 0x0 an interrupt is generated
    TA0CCTL0 |= CCIE;

    WRITE_SR(CPUOFF | GIE);     // set cpu in LPM0

    for(;;);
    return 0;
}


void __attribute__((interrupt(TIMER0_A0_VECTOR))) Timer0_A0_Int()
{
    int neg = 0, bcd = 0;
    TA0CTL &= ~1;       // clear timer interrupt flag
    if(dr_counter > DISP_T){
        dr_counter = 0;
        if(du_counter > 250){
            du_counter = 0;
            if(rxdata[0] & 0x80){   // negative data
                rxdata[0] = ~rxdata[0]; // convert to unsigned
                neg = 1;    // set negative sign flag
            }
            // convert binary value to 4 decimal digits on 16 bit
            bcd = bin2dec(rxdata[0]);
            // calculate fractional part
            if(rxdata[1] & 0x80)
                dd[0] = 5;  // 2^-1
            else
                dd[0] = 0;
            if(rxdata[1] & 0x40)
                dd[0] += 3; // 2^-2
            if(rxdata[1] & 0x20)
                dd[0]++;    // 2^-3
            if(rxdata[1] & 0x10 && ((rxdata[1] & 0x40) == 0))
                dd[0]++;    // 2^-4 = 0,0625, so add 1 only if 2^-2 bit is 0
            dd[1] = bcd & 0x0F;             // ones
            dd[2] = (bcd & 0x0F0) >> 4;     // tens
            if(dd[2]){                      // 2-nd digit isn't zero
                if(neg){                    // and displayed value is negative
                    dd[0] = 10 - (dd[0]);   // convert fractional part
                    dd[3] = MINUS;          // set minus sign on 3-rd digit
                }
                else
                    dd[3] = D_OFF;          //  otherwise 3-rd digit is off
            }
            else{
                if(neg)                     // negative value
                    dd[2] = MINUS;          // minus sign on 2-nd digit
                else
                    dd[2] = D_OFF;          // 2-nd digit is off
            }
        }
        P1OUT |= CATS;      // all display digits off
        P1OUT &= ~P1BITS;   // port 1 segments off
        P2OUT &= ~P2BITS;   // port 2 segments off
        digit <<= 1;        // select next digit
        if(digit > 8)
            digit = 1;      // back to first digit
        switch(digit){
        case 1:
            n = 3;
            break;
        case 2:
            n = 2;
            break;
        case 4:
            n = 1;
            break;
        case 8:
            n = 0;
            break;
        default:
            break;
        };
        P1OUT |= P1BITS & num1[dd[n]];  // set segments on port 1
        P2OUT |= P2BITS & num2[dd[n]];  // set segments on port 2
        if(n == 1)
            P2OUT |= dp;    // display dot point at the third digit
        P1OUT &= ~digit;    // turn on the selected digit
    }
    if(tx_counter > 150){   // set reading from temperature regiser
        tx_counter = 0;
        if(I2C_Tx == 0 && I2C_Rx == 0){ // no tx and no rx operation
            I2C_SetSA(TMP102);      // change slave address
            txdata[0] = TMP102_TREG;
            I2C_Write((char *)txdata, 1);   // send 1 byte
        }
    }
    if(rx_counter > 200){    // read temperature each
        rx_counter = 0;
        tx_counter = 0;
        if(I2C_Tx == 0 && I2C_Rx == 0){ // no tx and no rx operation
            //I2C_SetSA(TMP102);    // change slave address
            rxdata[0] = 0;
            rxdata[1] = 0;
            I2C_Read((char *)rxdata, 2);    // read 2 bytes
        }
    }
    // increment counters
    dr_counter++;
    du_counter++;
    tx_counter++;
    rx_counter++;
}
