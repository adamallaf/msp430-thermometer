/***********************
 * date:    14.06.2018
 * Author:  Adam Allaf
 ***********************/
	.file   "tmp102_init.s"
	.arch   msp430g2553
	.cpu    430
	.mpy    none

	.text
.global     TMP102_Init
/***********************
 * Function `TMP102_Init'
 * void TMP102_Init();
 ***********************/
TMP102_Init:
    MOV     #txdata, r15    ; pointer to txdata tab
    MOV.B   #1, @r15        ; txdata[0] = TMP102_CONFREG
    MOV.B   #96, 1(r15)     ; txdata[1] = RES12BITS
    MOV.B   #160, 2(r15)    ; txdata[2] = [CR1 CR0 AL EM] = 0xA;
                            ; => 4Hz conversion rate
    MOV     #3, r14         ; write 3 bytes
    CALL    #I2C_Write      ; send data to TMP102 over I2C
    MOV     #0, r15
.L1:
    CMP     r15, &I2C_Tx    ; check if data is sent (I2C_Tx = 0)
    JNE     .L1             ; loop until data is sent
    MOV.B   #0, &txdata     ; txdata[0] = TMP102_TREG
    MOV     #1, r14         ; send 1 byte
    MOV     #txdata, r15    ; pass txdata address to I2C_Write
    CALL    #I2C_Write      ; send data
    MOV     #0, r15
.L2:
    CMP     r15, &I2C_Tx
    JNE     .L2             ; loop until data is sent
    RET
;; End of function

.global     rxdata
    .section    .bss
rxdata:
    .skip   6,0     ; rxdata 6 bytes 0 value each
.global     txdata
txdata:
    .skip   6,0     ; txdata 6 bytes
