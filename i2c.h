/*
*   date:   02.04.2016
*   update: 17.04.2016
*
*   Author: Adam Allaf
*
*/
#ifndef __I2C_H__
#define __I2C_H__

void I2C_Init(int );

void I2C_SetSA(int );

void I2C_Write(char *, int );

void I2C_Read(char *, int );

#endif

