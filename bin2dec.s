/***********************
 * date:    18.04.2016
 * Athor:   Adam Allaf
 ***********************/
    .file   "bin2dec.s"
    .arch   msp430g2553
    .cpu    430
    .mpy    none

    .text
.global	bin2dec
/***********************
 * Function `bin2dec'
 * int bin2dec(int );
 ***********************/
bin2dec:
    MOV     #16, r14    ; init loop counter
    CLR     r13         ; r13 = 0
.L0:
    RLA     r15         ; left shift input argument
    DADD    r13, r13    ; add decimally with carry
    DEC     r14         ; r14--
    JNZ     .L0         ; repeat until r14 = 0
    MOV     r13, r15    ; set return value
    RET                 ; return
;; End of function

