/*
*   date:   02.04.2016
*   update: 17.04.2016
*
*   Author: Adam Allaf
*
*/
#include <msp430.h>
#include "i2c.h"


volatile unsigned int I2C_Tx, I2C_Rx;

static char *TxData, *RxData;

static int dataCtr;


void __attribute__((interrupt(USCIAB0TX_VECTOR))) I2CTX_Int()
{
    if(I2C_Tx){
        UCB0TXBUF = *TxData++;  // UCB0TXIFG is automatically cleared
        dataCtr--;
        if(dataCtr == 0){
            I2C_Tx = 0;
        }
    }
    else if(I2C_Rx){
        *RxData++ = UCB0RXBUF;  // UCB0RXIFG is automatically cleared
        dataCtr--;
        if(dataCtr == 0){
            I2C_Rx = 0;
        }
    }
    else{
        UCB0TXBUF = 0;
        UCB0CTL1 |= UCTXSTP;
    }
}


void I2C_Init(int slaveAddr){
    // P1.6 - SCL
    // P1.7 - SDA
    P1SEL |= BIT6 | BIT7;
    P1SEL2 |= BIT6 | BIT7;

    // UCSSEL_3 - USCI clock source, SMCLK as source (bits 7 & 6)
    // UCSWRST - enabled, USCI logic held in reset (bit 0).
    UCB0CTL1 = UCSSEL_3 | UCSWRST;      // 0x81,  BRCLK = SMCLK = 1MHz
    // UCMST - Master mode (bit 3)
    // UCMODE_3 - I2C mode (bits 1, 2)
    // UCSYNC - Synchronous mode (bit 0)
    UCB0CTL0 = UCMST | UCMODE_3 | UCSYNC;   // 0x0F
    // BRCLK prescaler
    // The 16-bit value of (UCBxBR0 + UCBxBR1 * 256) forms the prescaler value.
    UCB0BR1 = 0;
    UCB0BR0 = 10;               // f_i2c = f_BRCLK / UCBRx = 1MHz / 10 = 100kHz
    // UCGCEN - general call response enable (bit 15)
    // I2C own address (bits 9 down to 0)
    UCB0I2COA = 0;
    // I2C slave address (bits 9 down to 0)
    UCB0I2CSA = slaveAddr;      // 7-bit address
    UCB0CTL1 &= ~UCSWRST;       // release from reset
    UC0IE = UCB0TXIE;           // enable transmit interrupt
}


void I2C_SetSA(int slaveAddr){
    UCB0CTL1 |= UCSWRST;        // UCSI in reset mode
    // I2C slave address (bits 9 down to 0)
    UCB0I2CSA = slaveAddr;      // 7-bit address
    UCB0CTL1 &= ~UCSWRST;       // release from reset
}


void I2C_Write(char *data, int len){
    UC0IE &= ~UCB0RXIE;         // disable receive interrupt
    TxData = data;
    dataCtr = len;
    I2C_Tx = 1;
    UC0IE = UCB0TXIE;           // enable transmit interrupt
    UCB0CTL1 |= UCTR | UCTXSTT; // transmitter mode, I2C start condition
}


void I2C_Read(char *data, int len){
    UC0IE &= ~UCB0TXIE;         // disable transmit interrupt
    RxData = data;
    dataCtr = len;
    I2C_Rx = 1;
    UC0IE = UCB0RXIE;           // enable receive interrupt
    UCB0CTL1 &= ~UCTR;          // receiver mode
    UCB0CTL1 |= UCTXSTT;        // I2C start condition
}
